<?php declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180713184010 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_article_method_image DROP FOREIGN KEY FK_445BFB6A7E3C61F9');
        $this->addSql('ALTER TABLE Article DROP FOREIGN KEY FK_CD8737FA12469DE2');
        $this->addSql('CREATE TABLE HomeTranslation (id INT AUTO_INCREMENT NOT NULL, list_spotify VARCHAR(255) NOT NULL, selection_month LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Home (id INT AUTO_INCREMENT NOT NULL, link_lista VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE Article');
        $this->addSql('DROP TABLE CategoryArticle');
        $this->addSql('DROP TABLE app_article_method_image');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Article (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, content LONGTEXT NOT NULL COLLATE utf8_unicode_ci, link LONGTEXT NOT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) NOT NULL, INDEX IDX_CD8737FA12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE CategoryArticle (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_article_method_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, path VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_445BFB6A7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Article ADD CONSTRAINT FK_CD8737FA12469DE2 FOREIGN KEY (category_id) REFERENCES CategoryArticle (id)');
        $this->addSql('ALTER TABLE app_article_method_image ADD CONSTRAINT FK_445BFB6A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES Article (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE HomeTranslation');
        $this->addSql('DROP TABLE Home');
    }
}
