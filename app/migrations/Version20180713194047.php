<?php declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180713194047 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE HomeTranslation ADD translatable_id INT NOT NULL, ADD locale VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE HomeTranslation ADD CONSTRAINT FK_A8B455AE2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES Home (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_A8B455AE2C2AC5D3 ON HomeTranslation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX HomeTranslation_uniq_trans ON HomeTranslation (translatable_id, locale)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE HomeTranslation DROP FOREIGN KEY FK_A8B455AE2C2AC5D3');
        $this->addSql('DROP INDEX IDX_A8B455AE2C2AC5D3 ON HomeTranslation');
        $this->addSql('DROP INDEX HomeTranslation_uniq_trans ON HomeTranslation');
        $this->addSql('ALTER TABLE HomeTranslation DROP translatable_id, DROP locale');
    }
}
