<?php declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180713201953 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE HomeTranslation DROP FOREIGN KEY FK_A8B455AE2C2AC5D3');
        $this->addSql('CREATE TABLE app_home_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, list_spotify VARCHAR(255) NOT NULL, selection_month LONGTEXT NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_9D9B99832C2AC5D3 (translatable_id), UNIQUE INDEX app_home_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_article_translation (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, content LONGTEXT NOT NULL, list_spotify VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_home (id INT AUTO_INCREMENT NOT NULL, link_lista VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_home_translation ADD CONSTRAINT FK_9D9B99832C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_home (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE Home');
        $this->addSql('DROP TABLE HomeTranslation');
        $this->addSql('ALTER TABLE app_article ADD link_social_network LONGTEXT NOT NULL, ADD link_spotify LONGTEXT NOT NULL, DROP title, DROP description, DROP content, DROP link');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_home_translation DROP FOREIGN KEY FK_9D9B99832C2AC5D3');
        $this->addSql('CREATE TABLE Home (id INT AUTO_INCREMENT NOT NULL, link_lista VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE HomeTranslation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, list_spotify VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, selection_month LONGTEXT NOT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX HomeTranslation_uniq_trans (translatable_id, locale), INDEX IDX_A8B455AE2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE HomeTranslation ADD CONSTRAINT FK_A8B455AE2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES Home (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE app_home_translation');
        $this->addSql('DROP TABLE app_article_translation');
        $this->addSql('DROP TABLE app_home');
        $this->addSql('ALTER TABLE app_article ADD title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD content LONGTEXT NOT NULL COLLATE utf8_unicode_ci, ADD link LONGTEXT NOT NULL COLLATE utf8_unicode_ci, DROP link_social_network, DROP link_spotify');
    }
}
