<?php declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180713211432 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_article DROP FOREIGN KEY FK_EF678E2B12469DE2');
        $this->addSql('CREATE TABLE app_category_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_E8A28FD32C2AC5D3 (translatable_id), UNIQUE INDEX app_category_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_category (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_category_translation ADD CONSTRAINT FK_E8A28FD32C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_category (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE app_category_article');
        $this->addSql('ALTER TABLE app_article DROP FOREIGN KEY FK_EF678E2B12469DE2');
        $this->addSql('ALTER TABLE app_article ADD CONSTRAINT FK_EF678E2B12469DE2 FOREIGN KEY (category_id) REFERENCES app_category (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_category_translation DROP FOREIGN KEY FK_E8A28FD32C2AC5D3');
        $this->addSql('ALTER TABLE app_article DROP FOREIGN KEY FK_EF678E2B12469DE2');
        $this->addSql('CREATE TABLE app_category_article (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE app_category_translation');
        $this->addSql('DROP TABLE app_category');
        $this->addSql('ALTER TABLE app_article DROP FOREIGN KEY FK_EF678E2B12469DE2');
        $this->addSql('ALTER TABLE app_article ADD CONSTRAINT FK_EF678E2B12469DE2 FOREIGN KEY (category_id) REFERENCES app_category_article (id)');
    }
}
