<?php declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180716194408 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_article ADD home_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE app_article ADD CONSTRAINT FK_EF678E2B28CDC89C FOREIGN KEY (home_id) REFERENCES app_home (id)');
        $this->addSql('CREATE INDEX IDX_EF678E2B28CDC89C ON app_article (home_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_article DROP FOREIGN KEY FK_EF678E2B28CDC89C');
        $this->addSql('DROP INDEX IDX_EF678E2B28CDC89C ON app_article');
        $this->addSql('ALTER TABLE app_article DROP home_id');
    }
}
