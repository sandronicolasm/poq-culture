<?php

namespace AppBundle\Controller\Shop;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends Controller
{
    public function privacyAction(Request $request): Response
    {
        return $this->render('');
    }
}