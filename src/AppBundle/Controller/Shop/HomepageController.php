<?php

namespace AppBundle\Controller\Shop;

<<<<<<< HEAD
=======
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

<<<<<<< HEAD
final class HomepageController
=======
final class HomepageController extends Controller
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
{
    /**
     * @var EngineInterface
     */
    private $templatingEngine;

    /**
     * @param EngineInterface $templatingEngine
     */
    public function __construct(EngineInterface $templatingEngine)
    {
        $this->templatingEngine = $templatingEngine;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        $manager = $this->container->get('app.repository.home');
        $home_data = $manager->findAll();
        $manager_categoires = $this->container->get('app.repository.category');
        $categories = $manager_categoires->findAll();
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig', [
            'home_data' => $home_data[0],
            'categories' => $categories
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function articlesByCategoryAction(Request $request, $category): Response
    {
        $manager = $this->container->get('app.repository.article');
        $articles = $manager->findBy(array('category' => $category));
        $manager_categoires = $this->container->get('app.repository.category');
        $categories = $manager_categoires->findAll();
        dump($articles);

        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/magazine.html.twig', [
            'articles' => $articles,
            'categories' => $categories
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function productsByCategoryAction(Request $request, $taxon): Response
    {
//        $manager = $this->container->get('sylius.repository.taxon');
//        $taxon_m = $manager->findOneBy(array('id' => $taxon));
//        dump($taxon_m);
        $manager = $this->container->get('sylius.repository.product');
        $products = $manager->findBy(array('mainTaxon' => $taxon));
        $manager_categoires = $this->container->get('sylius.repository.taxon');
        $categories = $manager_categoires->findAll();

        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/shop.html.twig', [
            'products' => $products,
            'categories' => $categories
        ]);
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function cultureAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/privacy_policy.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/poq_culture.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function magazineAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        $manager = $this->get('app.repository.article');
        $articles = $manager->findBy(array('is_active' => true));
        $manager = $this->get('app.repository.category');
        $categories = $manager->findAll();

        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/magazine.html.twig', array(
            'categories' => $categories,
            'articles' => $articles
        ));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function articleByIdAction(Request $request, $article): Response
    {
        $manager = $this->get('app.repository.article');
        $article = $manager->find($article);

        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/article.html.twig', array(
            'article' => $article
        ));
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function shopAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        $manager = $this->get('sylius.repository.product');
        $products = $manager->findAll();
        $manager_categoires = $this->container->get('sylius.repository.taxon');
        $categories = $manager_categoires->findAll();
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/shop.html.twig',
            array(
                'products' => $products,
                'categories' => $categories
            ));
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function aboutAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/about.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function faqAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/faqs.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function privacyAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/privacy_policy.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function shippingAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/shipping.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function confidentialityAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/confidentiality.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function legalAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/legal.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function termsAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/terms.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function cookiesAction(Request $request): Response
    {
<<<<<<< HEAD
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/index.html.twig');
=======
        return $this->templatingEngine->renderResponse('@SyliusShop/Homepage/privacy_cookies.html.twig');
>>>>>>> 4dc86fc213c30ca942131a3d280f4d0ae9ab4cb2
    }
}