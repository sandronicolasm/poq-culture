<?php

namespace AppBundle\Entity;

use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\String_;
use Sylius\Component\Resource\Model\ResourceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;


/**
 * Article
 */
class Article implements ResourceInterface, TranslatableInterface , ImagesAwareInterface
{
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
    }

    public function __construct()
    {
        $this->initializeTranslationsCollection();

        $this->images = new ArrayCollection();
        $this->addImage(new ArticleImage());
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $link_social_network;

    /**
     * @var string
     */
    private $link_spotify;

    /**
     * @var string
     */
    private $list_spotify;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $name_social_network;

    /**
     * @var boolean
     */
    private $is_active;


    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->getTranslation()->setTitle($title);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getTranslation()->getTitle();
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->getTranslation()->setDescription($description);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getTranslation()->getDescription();
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->getTranslation()->setContent($content);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getTranslation()->getContent();
    }

    /**
     * @param string $listSpotify
     */
    public function setListSpotify($listSpotify)
    {
        $this->list_spotify = $listSpotify;
    }

    /**
     * @return string
     */
    public function getListSpotify()
    {
        return $this->list_spotify;
    }

    /**
     * @var Collection|ImageInterface[]
     */
    protected $images;


    /**
     * {@inheritdoc}
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getImagesByType(string $type): Collection
    {
        return $this->images->filter(function (ImageInterface $image) use ($type) {
            return $type === $image->getType();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function hasImages(): bool
    {
        return !$this->images->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasImage(ImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    /**
     * {@inheritdoc}
     */
    public function addImage(ImageInterface $image): void
    {
        $image->setOwner($this);
        $this->images->add($image);
    }

    /**
     * {@inheritdoc}
     */
    public function removeImage(ImageInterface $image): void
    {
        if ($this->hasImage($image)) {
            $image->setOwner(null);
            $this->images->removeElement($image);
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return Article
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * {@inheritdoc}
     */
    protected function createTranslation()
    {
        return new ArticleTranslation();
    }

    /**
     * Set linkSocialNetwork.
     *
     * @param string $linkSocialNetwork
     *
     * @return Article
     */
    public function setLinkSocialNetwork($linkSocialNetwork)
    {
        $this->link_social_network = $linkSocialNetwork;

        return $this;
    }

    /**
     * Get linkSocialNetwork.
     *
     * @return string
     */
    public function getLinkSocialNetwork()
    {
        return $this->link_social_network;
    }

    /**
     * Set linkSpotify.
     *
     * @param string $linkSpotify
     *
     * @return Article
     */
    public function setLinkSpotify($linkSpotify)
    {
        $this->link_spotify = $linkSpotify;

        return $this;
    }

    /**
     * Get linkSpotify.
     *
     * @return string
     */
    public function getLinkSpotify()
    {
        return $this->link_spotify;
    }

    /**
     * @var \AppBundle\Entity\Category
     */
    private $category;

    /**
     * Set category.
     *
     * @param \AppBundle\Entity\Category|null $category
     *
     * @return Article
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \AppBundle\Entity\Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function __toString()
    {
        return (string) $this->getTranslation()->getTitle();
    }

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_one;


    /**
     * Set homeOne.
     *
     * @param \AppBundle\Entity\Home|null $homeOne
     *
     * @return Article
     */
    public function setHomeOne(\AppBundle\Entity\Home $homeOne = null)
    {
        $this->home_one = $homeOne;

        return $this;
    }

    /**
     * Get homeOne.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeOne()
    {
        return $this->home_one;
    }
    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_two;


    /**
     * Set homeTwo.
     *
     * @param \AppBundle\Entity\Home|null $homeTwo
     *
     * @return Article
     */
    public function setHomeTwo(\AppBundle\Entity\Home $homeTwo = null)
    {
        $this->home_two = $homeTwo;

        return $this;
    }

    /**
     * Get homeTwo.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeTwo()
    {
        return $this->home_two;
    }
    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_three;



    /**
     * Set homeThree.
     *
     * @param \AppBundle\Entity\Home|null $homeThree
     *
     * @return Article
     */
    public function setHomeThree(\AppBundle\Entity\Home $homeThree = null)
    {
        $this->home_three = $homeThree;

        return $this;
    }

    /**
     * Get homeThree.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeThree()
    {
        return $this->home_three;
    }

    /**
     * Set nameSocialNetwork.
     *
     * @param string $nameSocialNetwork
     *
     * @return Article
     */
    public function setNameSocialNetwork($nameSocialNetwork)
    {
        $this->name_social_network = $nameSocialNetwork;

        return $this;
    }

    /**
     * Get nameSocialNetwork.
     *
     * @return string
     */
    public function getNameSocialNetwork()
    {
        return $this->name_social_network;
    }
}
