<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Sylius\Component\Core\Model\ImageInterface;


/**
 * Home
 */
class Home implements ResourceInterface, TranslatableInterface, ImagesAwareInterface
{
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
    }

    public function __construct()
    {
        $this->initializeTranslationsCollection();

        $this->images = new ArrayCollection();
//        $this->addImage(new HomeImage());
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $linkLista;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set linkLista.
     *
     * @param string $linkLista
     *
     * @return Home
     */
    public function setLinkLista($linkLista)
    {
        $this->linkLista = $linkLista;

        return $this;
    }

    /**
     * Get linkLista.
     *
     * @return string
     */
    public function getLinkLista()
    {
        return $this->linkLista;
    }

    /**
     * @param string $listSpotify
     */
    public function setListSpotify($listSpotify)
    {
        $this->getTranslation()->setListSpotify($listSpotify);
    }

    /**
     * @return string
     */
    public function getListSpotify()
    {
        return $this->getTranslation()->getListSpotify();
    }

    /**
     * @param string $selectionMonth
     */
    public function setSelectionMonth($selectionMonth)
    {
        $this->getTranslation()->setSelectionMonth($selectionMonth);
    }

    /**
     * @return string
     */
    public function getSelectionMonth()
    {
        return $this->getTranslation()->getSelectionMonth();
    }


    /**
     * {@inheritdoc}
     */
    protected function createTranslation()
    {
        return new HomeTranslation();
    }

    /**
     * @var \AppBundle\Entity\Article
     */
    private $article_one;


    /**
     * Set articleOne.
     *
     * @param \AppBundle\Entity\Article|null $articleOne
     *
     * @return Home
     */
    public function setArticleOne(\AppBundle\Entity\Article $articleOne = null)
    {
        $this->article_one = $articleOne;

        return $this;
    }

    /**
     * Get articleOne.
     *
     * @return \AppBundle\Entity\Article|null
     */
    public function getArticleOne()
    {
        return $this->article_one;
    }
    /**
     * @var \AppBundle\Entity\Article
     */
    private $article_two;


    /**
     * Set articleTwo.
     *
     * @param \AppBundle\Entity\Article|null $articleTwo
     *
     * @return Home
     */
    public function setArticleTwo(\AppBundle\Entity\Article $articleTwo = null)
    {
        $this->article_two = $articleTwo;

        return $this;
    }

    /**
     * Get articleTwo.
     *
     * @return \AppBundle\Entity\Article|null
     */
    public function getArticleTwo()
    {
        return $this->article_two;
    }
    /**
     * @var \AppBundle\Entity\Article
     */
    private $article_three;



    /**
     * Set articleThree.
     *
     * @param \AppBundle\Entity\Article|null $articleThree
     *
     * @return Home
     */
    public function setArticleThree(\AppBundle\Entity\Article $articleThree = null)
    {
        $this->article_three = $articleThree;

        return $this;
    }

    /**
     * Get articleThree.
     *
     * @return \AppBundle\Entity\Article|null
     */
    public function getArticleThree()
    {
        return $this->article_three;
    }

    /**
     * @var Collection|ImageInterface[]
     */
    protected $images;


    /**
     * {@inheritdoc}
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getImagesByType(string $type): Collection
    {
        return $this->images->filter(function (ImageInterface $image) use ($type) {
            return $type === $image->getType();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function hasImages(): bool
    {
        return !$this->images->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasImage(ImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    /**
     * {@inheritdoc}
     */
    public function addImage(ImageInterface $image): void
    {
        $image->setOwner($this);
        $this->images->add($image);
    }

    /**
     * {@inheritdoc}
     */
    public function removeImage(ImageInterface $image): void
    {
        if ($this->hasImage($image)) {
            $image->setOwner(null);
            $this->images->removeElement($image);
        }
    }
    /**
     * @var \AppBundle\Entity\Product
     */
    private $product_one;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product_two;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product_three;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product_four;


    /**
     * Set productOne.
     *
     * @param \AppBundle\Entity\Product|null $productOne
     *
     * @return Home
     */
    public function setProductOne(\AppBundle\Entity\Product $productOne = null)
    {
        $this->product_one = $productOne;

        return $this;
    }

    /**
     * Get productOne.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProductOne()
    {
        return $this->product_one;
    }

    /**
     * Set productTwo.
     *
     * @param \AppBundle\Entity\Product|null $productTwo
     *
     * @return Home
     */
    public function setProductTwo(\AppBundle\Entity\Product $productTwo = null)
    {
        $this->product_two = $productTwo;

        return $this;
    }

    /**
     * Get productTwo.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProductTwo()
    {
        return $this->product_two;
    }

    /**
     * Set productThree.
     *
     * @param \AppBundle\Entity\Product|null $productThree
     *
     * @return Home
     */
    public function setProductThree(\AppBundle\Entity\Product $productThree = null)
    {
        $this->product_three = $productThree;

        return $this;
    }

    /**
     * Get productThree.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProductThree()
    {
        return $this->product_three;
    }

    /**
     * Set productFour.
     *
     * @param \AppBundle\Entity\Product|null $productFour
     *
     * @return Home
     */
    public function setProductFour(\AppBundle\Entity\Product $productFour = null)
    {
        $this->product_four = $productFour;

        return $this;
    }

    /**
     * Get productFour.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProductFour()
    {
        return $this->product_four;
    }
    /**
     * @var \AppBundle\Entity\Product
     */
    private $product_big_one;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product_big_two;


    /**
     * Set productBigOne.
     *
     * @param \AppBundle\Entity\Product|null $productBigOne
     *
     * @return Home
     */
    public function setProductBigOne(\AppBundle\Entity\Product $productBigOne = null)
    {
        $this->product_big_one = $productBigOne;

        return $this;
    }

    /**
     * Get productBigOne.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProductBigOne()
    {
        return $this->product_big_one;
    }

    /**
     * Set productBigTwo.
     *
     * @param \AppBundle\Entity\Product|null $productBigTwo
     *
     * @return Home
     */
    public function setProductBigTwo(\AppBundle\Entity\Product $productBigTwo = null)
    {
        $this->product_big_two = $productBigTwo;

        return $this;
    }

    /**
     * Get productBigTwo.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProductBigTwo()
    {
        return $this->product_big_two;
    }
}
