<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\AbstractTranslation;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * HomeTranslation
 */
class HomeTranslation extends AbstractTranslation implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $listSpotify;

    /**
     * @var string
     */
    private $selectionMonth;

    protected $locale;

    protected $translatable;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listSpotify.
     *
     * @param string $listSpotify
     *
     * @return HomeTranslation
     */
    public function setListSpotify($listSpotify)
    {
        $this->listSpotify = $listSpotify;

        return $this;
    }

    /**
     * Get listSpotify.
     *
     * @return string
     */
    public function getListSpotify()
    {
        return $this->listSpotify;
    }

    /**
     * Set selectionMonth.
     *
     * @param string $selectionMonth
     *
     * @return HomeTranslation
     */
    public function setSelectionMonth($selectionMonth)
    {
        $this->selectionMonth = $selectionMonth;

        return $this;
    }

    /**
     * Get selectionMonth.
     *
     * @return string
     */
    public function getSelectionMonth()
    {
        return $this->selectionMonth;
    }
}
