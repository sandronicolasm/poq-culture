<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product as BaseCountry;

class Product extends BaseCountry
{
    public function __construct() {
        parent::__construct();
    }

    /**
     * @var string
     */
    private $title_spotify;

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_product_one;

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_product_two;

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_product_three;

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_product_four;

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_big_one;

    /**
     * @var \AppBundle\Entity\Home
     */
    private $home_big_two;

    /**
     * @return string|null
     */
    public function getTitleSpotify(): ?string
    {
        return $this->title_spotify;
    }

    /**
     * @param string $title_spotify
     */
    public function setTitleSpotify(string $titleSpotify): void
    {
        $this->title_spotify = $titleSpotify;
    }

    /**
     * Set homeProductOne.
     *
     * @param \AppBundle\Entity\Home|null $homeProductOne
     *
     * @return Product
     */
    public function setHomeProductOne(\AppBundle\Entity\Home $homeProductOne = null)
    {
        $this->home_product_one = $homeProductOne;

        return $this;
    }

    /**
     * Get homeProductOne.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeProductOne()
    {
        return $this->home_product_one;
    }

    /**
     * Set homeProductTwo.
     *
     * @param \AppBundle\Entity\Home|null $homeProductTwo
     *
     * @return Product
     */
    public function setHomeProductTwo(\AppBundle\Entity\Home $homeProductTwo = null)
    {
        $this->home_product_two = $homeProductTwo;

        return $this;
    }

    /**
     * Get homeProductTwo.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeProductTwo()
    {
        return $this->home_product_two;
    }

    /**
     * Set homeProductThree.
     *
     * @param \AppBundle\Entity\Home|null $homeProductThree
     *
     * @return Product
     */
    public function setHomeProductThree(\AppBundle\Entity\Home $homeProductThree = null)
    {
        $this->home_product_three = $homeProductThree;

        return $this;
    }

    /**
     * Get homeProductThree.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeProductThree()
    {
        return $this->home_product_three;
    }

    /**
     * Set homeProductFour.
     *
     * @param \AppBundle\Entity\Home|null $homeProductFour
     *
     * @return Product
     */
    public function setHomeProductFour(\AppBundle\Entity\Home $homeProductFour = null)
    {
        $this->home_product_four = $homeProductFour;

        return $this;
    }

    /**
     * Get homeProductFour.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeProductFour()
    {
        return $this->home_product_four;
    }

    /**
     * Set homeBigOne.
     *
     * @param \AppBundle\Entity\Home|null $homeBigOne
     *
     * @return Product
     */
    public function setHomeBigOne(\AppBundle\Entity\Home $homeBigOne = null)
    {
        $this->home_big_one = $homeBigOne;

        return $this;
    }

    /**
     * Get homeBigOne.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeBigOne()
    {
        return $this->home_big_one;
    }

    /**
     * Set homeBigTwo.
     *
     * @param \AppBundle\Entity\Home|null $homeBigTwo
     *
     * @return Product
     */
    public function setHomeBigTwo(\AppBundle\Entity\Home $homeBigTwo = null)
    {
        $this->home_big_two = $homeBigTwo;

        return $this;
    }

    /**
     * Get homeBigTwo.
     *
     * @return \AppBundle\Entity\Home|null
     */
    public function getHomeBigTwo()
    {
        return $this->home_big_two;
    }
}
