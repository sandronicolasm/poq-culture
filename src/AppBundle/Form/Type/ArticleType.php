<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;


class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listSpotify', TextType::class, [
                'required' => true,
                'label' => 'app.ui.list_spotify'
            ])
            ->add('linkSpotify', TextType::class, [
                'required' => true,
                'label' => 'app.ui.link_lista'
            ])
            ->add('linkSocialNetwork', TextType::class, [
                'required' => true,
                'label' => 'app.ui.link_social_network'
            ])
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ArticleTranslationType::class,
            ])
            ->add('images', CollectionType::class, [
		    'entry_type' => ArticleImageType::class,
		    'allow_add' => false,
		    'allow_delete' => false,
		    'by_reference' => false,
		    'label' => 'app.ui.images',
            ])
            ->add('category', null, [
                'required' => true,
                'label' => 'app.ui.category'
            ])
            ->add('is_active')
            ->add('nameSocialNetwork', ChoiceType::class, array(
                'choices'  => array(
                    'Instagram' => 'instagram',
                    'Facebook' => 'facebook',
                ),
                'multiple' => false,
                'expanded' => true,
                'required' => true
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_article';
    }


}
