<?php

namespace AppBundle\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class HomeTranslationType extends AbstractResourceType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listSpotify', TextType::class,
                [
                    'required' => true,
                    'label' => 'app.ui.list_spotify'
                ])
            ->add('selectionMonth', TextareaType::class,
                [
                    'required' => true,
                    'label' => 'app.ui.selected_month'
                ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_home_tranlation';
    }


}
