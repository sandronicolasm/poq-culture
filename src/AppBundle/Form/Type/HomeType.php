<?php

namespace AppBundle\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class HomeType extends AbstractResourceType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => HomeTranslationType::class,
            ])
            ->add('linkLista', TextType::class, [
                'required' => true,
                'label' => 'app.ui.link_lista'
            ])
            ->add('article_one', EntityType::class,[
                'class' => 'AppBundle:Article',
                'choice_label' => 'title',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_article_one'
            ])
            ->add('article_two', EntityType::class,[
                'class' => 'AppBundle:Article',
                'choice_label' => 'title',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_article_two'
            ])
            ->add('article_three', EntityType::class,[
                'class' => 'AppBundle:Article',
                'choice_label' => 'title',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_article_three'
            ])
            ->add('product_big_one', EntityType::class,[
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_product_big_one'
            ])
            ->add('product_big_two', EntityType::class,[
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_product_big_two'
            ])
            ->add('product_one', EntityType::class,[
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_product_one'
            ])
            ->add('product_two', EntityType::class,[
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_product_two'
            ])
            ->add('product_three', EntityType::class,[
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_product_three'
            ])
            ->add('product_four', EntityType::class,[
                'class' => 'AppBundle:Product',
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.ui.home_product_four'
            ])

            ->add('images', CollectionType::class, [
                'entry_type' => HomeImageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'app.ui.images_home',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_home';
    }
}