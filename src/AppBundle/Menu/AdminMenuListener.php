<?php

namespace AppBundle\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $newSubmenu = $menu
            ->addChild('magazine')
            ->setLabel('Magazine')
        ;
        $menu['catalog']->addChild('home', array(
            'route' => 'app_admin_home_index',
            'labelAttributes' => array('icon' => 'home'),
        ))->setLabel('app.ui.homes');

        $newSubmenu->addChild('category', array(
            'route' => 'app_admin_category_index',
            'labelAttributes' => array('icon' => 'tag'),
        ))->setLabel('app.ui.categories');

        $newSubmenu->addChild('article', array(
            'route' => 'app_admin_article_index',
            'labelAttributes' => array('icon' => 'images'),
        ))->setLabel('app.ui.articles');
    }
}